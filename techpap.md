# Document Object Model 
### Udesh Kumar Chaudhary
Mountblue technology,
Bangalore

## 1 DOM
The Document Object Model (DOM) is a programming API for HTML and XML documents. The DOM represents the document as nodes and objects; that way, programming languages can interact with the page.

With the Document Object Model, programmers can create and build documents, navigate their structure, and add, modify, or delete elements and content.

According to the W3C, one of the main reasons for developing the DOM is to maintain a standard programming interface for HTML and XML documents, which is language-independent.

## 2 How does it help
The way I learned about DOM is like looking at a tree upside down. And each element in the document tree is called a Node.
![Document Tree](https://miro.medium.com/max/560/0*9tdJcsa3ipSNsR2w.png)

Most nodes have a starting tag and ending tag, but nodes like ‘img’ tag are self-closing, meaning you don’t need to add a </img> at the end of the line, instead you do it like ‘<img src= “my image link” />’

Things can be nested inside these tags. The inner node is called a child node and the outer node is considered to be its parent node.

## 3 How to access it

Structurally, the Document Object Model consists of nodes, with each node representing content in the web document. It gives developers a way of representing everything on a web page so that the contents of the web page is accessible via a common set of properties and methods.

The code index.html file will serve as boilerplate for the upcoming examples.
```
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="ie=edge" />
<title>Javascript DOM Access</title>
</head>
<body>
THIS IS WHERE THE HTML GOES
<script>
THIS IS WHERE THE JAVASCRIPT WILL GO
</script>
</body>
</html>
```

#### .getElementsByTagName( )
This method will return an array of all the elements you specify between the parentheses.
```
<html>
 <p>Accessed by Tag</p>
 <p>Accessed by Tag</p>
</html>
<script>
 getByTag = document.getElementsByTagName("p");
</script>
```
#### .getElementsByClassName( )
This method uses the class attribute of an element in order to gain access to it.
```
<html>
 <button class="hello">Hello</button>
 <button>Good Morning</button>
 <button class="hello">Hello</button>
 <button>Click</button>
</html>
<script>
className = document.getElementsByClassName(".hello");
</script>
```
#### .getElementById( )
This method is perfect if you want to target one specific element.
```
<html>
 <button>Click Me</button>
 <button id="myButton">Click Me Also</button>
 <button>Click Me Again</button>
</html>
<script>
accessById = document.getElementById("myButton");
</script>
```

#### .querySelectorAll( )
We can use this method to collect all of the elements that match a specific query. 
```
<html>
 <p class="query-all">Hello World</p>
 <p>Good Morning</p>
 <p class="query-all">How is your day?</p>
</html>
<script>
queryAll = document.querySelectorAll(".query-all");
</script>
```
#### .querySelector( )
In a way this method is the counterpart to the previous method discussed.
```
<html>
 <button>Click Me</button>
 <button id="only-me">Click Me Also</button>
 <button>Purchase</button>
 <button class="back">Go Back</button>
</html>
<script>
onlyMe = document.querySelector("#only-me");
goBack = document.querySelector(".back");
</script>
```

## 4 What are JS Helper method
JS Helper methods are more precisely used for data manipulation, which was introduced in ES6 or ECMAScript 6.

The below code apply to all the example:
```
 { name: "Sam",      age: 25, role: "Developer" },
  { name: "John",     age: 32, role: "Manager"   },
  { name: "Ronaldo",  age: 29, role: "Architect" },
  { name: "Perker",   age: 25, role: "Developer" },
  { name: "Sophia",   age: 38, role: "Director"  },
  { name: "kristine", age: 21, role: "Developer" },
];
 ```
Let’s discuss some important methods.
#### 1. every() Method:
 This method is used to check if all elements of an array pass the test that is implemented by the passed higher-order function.

 ```
  const employees = [
 
function isDeveloper(employee) {
  return employee.role === "Developer";
}
console.log(employees.every(isDeveloper));
```
Output:
```
false
 ```
#### 2. fill() Method:
```       
const newEmployees = employees.fill(
    { name: "Sam", age: 25, role: "Developer" });
console.log(employees);
 
console.log(newEmployees === employees);  This method fills the array with a static value.
```

Output:
```
[
 { name: 'Sam', age: 25, role: 'Developer' },
 { name: 'Sam', age: 25, role: 'Developer' },
 { name: 'Sam', age: 25, role: 'Developer' },
 { name: 'Sam', age: 25, role: 'Developer' },
 { name: 'Sam', age: 25, role: 'Developer' },
 { name: 'Sam', age: 25, role: 'Developer' }
]

true
```
#### 3. filter() Method:
 This method filters the array that passes the test with the function passed to it. It returns a new array.
 ```       
function filterDevEmp(employee) {
  return employee.role === "Developer";
}
const filteredDevEmployees = employees.filter(filterDevEmp);
console.log(filteredDevEmployees);
```

Output:
```

[
 { name: 'Sam', age: 25, role: 'Developer' },
 { name: 'Perker', age: 25, role: 'Developer' },
 { name: 'kristine', age: 21, role: 'Developer' }
] 
```
#### 4. find() Method:
 This method returns the first element that passes the test with the provided function.
```
const employees = [
function searchFirstDevEmployee(employee) {
  return employee.role === "Developer";
}
const firstEmployeeDeveloper =
    employees.find(searchFirstDevEmployee);
 
console.log(firstEmployeeDeveloper);   
```
Output:
```
 { name: 'Sam', age: 25, role: 'Developer' }
 ```
 #### 5. forEach() Method: 
 This is one of the most used method. It is used to call or execute the provided/passed function once for each element in an array.
```     
function increaseAgeByOne(employee) {
  employee.age += 1;
}
employees.forEach(increaseAgeByOne);
console.log(employees);
```
Output:
```
[
 { name: 'Sam', age: 26, role: 'Developer' },
 { name: 'John', age: 33, role: 'Manager' },
 { name: 'Ronaldo', age: 30, role: 'Architect' },
 { name: 'Perker', age: 26, role: 'Developer' },
 { name: 'Sophia', age: 39, role: 'Director' },
 { name: 'kristine', age: 22, role: 'Developer' }
]
```
#### 6. includes() Method:
 This method is used to test whether an element is present in an array or not. 
```
console.log(employees.includes(arch));
```
Output:
```
true
```
#### 7. indexOf() Method:
 This method returns the first element index that passes the test with the provided function.
```
const names = ["Sam", "John", "Ronaldo",
    "Perker", "Sophia", "kristine"];
names.indexOf("John");   
names.indexOf("john");
```
#### 8. map() Method: 
This method call the provided function and execute this function for each element.
```
function getName(employee) {
  return employee.name;
}
 
const names = employees.map(getName);
console.log(names);   
 
function concetenateNameWithAge(employee) {
  return employee.name + " " + employee.age;
}
const nameWithAge = employees.map(concetenateNameWithAge);
console.log(nameWithAge);
```
```
Output:

 [ 'Sam', 'John', 'Ronaldo', 'Perker', 'Sophia', 'kristine' ]
[
 'Sam 25',
 'John 32',
 'Ronaldo 29',
 'Perker 25',
 'Sophia 38',
 'kristine 21'
]
```
#### 9. reduce() Method: 
The array reduce() method executes the reducer function that you pass on each element and it always returns a single value.
```
function getRoleReducer(acc, currentValue) {
  acc.push(currentValue.role);
  return acc;
}
const roles = employees.reduce(getRoleReducer, []);
console.log(roles);
```
Output:
```
[
 'Developer',
 'Manager',
 'Architect',
 'Developer',
 'Director',
 'Developer'
]
```
#### 10. some() Method: 
This method checks if any one of the elements in the array passes the test provided by the predicate function. 
```
function checkIfDevExist(employee) {
  return employee.role === "Developer";
}
const isDeveloperExist = employees.some(checkIfDevExist);
console.log(isDeveloperExist);
```
Output:
```
true
```
## References

1. DOM
* https://www.w3.org/TR/WD-DOM/introduction.html
* https://study.com/academy/lesson/the-document-object-model-dom-purpose-function.html
2. How does it help:
* https://medium.com/swlh/working-on-dom-nodes-and-its-properties-in-javascript-af3ac77f803
3. How to access it:
* https://medium.com/@ralph1786/accessing-dom-elements-with-javascript-7962f73c59be
* https://www.digitalocean.com/community/tutorials/how-to-access-elements-in-the-dom
4. What are JS Helper method:
* https://www.geeksforgeeks.org/javascript-helper-methods/
* https://medium.com/swlh/array-helper-methods-in-es6-28fc5e5a5dc9


